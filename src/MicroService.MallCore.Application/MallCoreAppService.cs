﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroService.MallCore.Localization;
using Volo.Abp.Application.Services;

namespace MicroService.MallCore
{
    /* Inherit your application services from this class.
     */
    public abstract class MallCoreAppService : ApplicationService
    {
        protected MallCoreAppService()
        {
            LocalizationResource = typeof(MallCoreResource);
        }
    }
}
