﻿using AutoMapper;

namespace MicroService.MallCore
{
    public class MallCoreApplicationAutoMapperProfile : Profile
    {
        public MallCoreApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
