﻿using MicroService.MallCore.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace MicroService.MallCore.Permissions
{
    public class MallCorePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(MallCorePermissions.GroupName);
            //Define your own permissions here. Example:
            //myGroup.AddPermission(MallCorePermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<MallCoreResource>(name);
        }
    }
}
