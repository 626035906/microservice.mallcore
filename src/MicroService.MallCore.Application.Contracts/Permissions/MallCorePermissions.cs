﻿namespace MicroService.MallCore.Permissions
{
    public static class MallCorePermissions
    {
        public const string GroupName = "MallCore";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}