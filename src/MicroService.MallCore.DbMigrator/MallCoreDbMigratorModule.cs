﻿using MicroService.MallCore.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace MicroService.MallCore.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(MallCoreEntityFrameworkCoreModule),
        typeof(MallCoreApplicationContractsModule)
        )]
    public class MallCoreDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
