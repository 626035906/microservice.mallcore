﻿using System.Threading.Tasks;

namespace MicroService.MallCore.Data
{
    public interface IMallCoreDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
