﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace MicroService.MallCore.Data
{
    /* This is used if database provider does't define
     * IMallCoreDbSchemaMigrator implementation.
     */
    public class NullMallCoreDbSchemaMigrator : IMallCoreDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}