﻿using Volo.Abp.Settings;

namespace MicroService.MallCore.Settings
{
    public class MallCoreSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(MallCoreSettings.MySetting1));
        }
    }
}
