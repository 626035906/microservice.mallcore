﻿using Volo.Abp.Localization;

namespace MicroService.MallCore.Localization
{
    [LocalizationResourceName("MallCore")]
    public class MallCoreResource
    {

    }
}