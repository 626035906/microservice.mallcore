﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MicroService.MallCore.Data;
using Volo.Abp.DependencyInjection;

namespace MicroService.MallCore.EntityFrameworkCore
{
    public class EntityFrameworkCoreMallCoreDbSchemaMigrator
        : IMallCoreDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreMallCoreDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the MallCoreDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<MallCoreDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}
