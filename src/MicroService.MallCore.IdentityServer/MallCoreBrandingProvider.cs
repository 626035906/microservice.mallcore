﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace MicroService.MallCore
{
    [Dependency(ReplaceServices = true)]
    public class MallCoreBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "MallCore";
    }
}
