﻿using MicroService.MallCore.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MicroService.MallCore.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class MallCoreController : AbpController
    {
        protected MallCoreController()
        {
            LocalizationResource = typeof(MallCoreResource);
        }
    }
}