﻿using MicroService.MallCore.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace MicroService.MallCore
{
    [DependsOn(
        typeof(MallCoreEntityFrameworkCoreTestModule)
        )]
    public class MallCoreDomainTestModule : AbpModule
    {

    }
}