﻿using Volo.Abp.Modularity;

namespace MicroService.MallCore
{
    [DependsOn(
        typeof(MallCoreApplicationModule),
        typeof(MallCoreDomainTestModule)
        )]
    public class MallCoreApplicationTestModule : AbpModule
    {

    }
}